/**
 * Created by stoez on 13.05.2016.
 */
var _rowsPerPage = 10;
var _nextSelector = '#next';
var _backSelector = '#prev';
var _baseUrl = "/score-server/rs/snake";
var wsUri = "ws://" + document.location.host + document.location.pathname + "socket";
var ws;
var startColor, endColor, yourColor;
var container;
var temp;

var barChartOptions = {
    chartPadding: 30,
    width: 500,
    height: 300,
    stretch: true
};

var lineChartOptions = {
    axisX: {
        showGrid: false,
        showLabel: false
    },
    chartPadding: 30,
    width: 500,
    height: 300,
    stretch: true,
    //showPoint: false,
    lineSmooth: Chartist.Interpolation.none()
};

var tooltipHover;
var tooltip = 0;

function leastCommonMultiple(arr) {
    var calculatedLCM = 1;
    for (let i = 0; i < arr.length; i++) {
        calculatedLCM = leastCommonMultipleOfTwo(calculatedLCM, arr[i]);
    }
    return calculatedLCM;
}

function greatestCommonDivisor(a, b) {
    return b ? greatestCommonDivisor(b, a % b) : Math.abs(a);
}

function leastCommonMultipleOfTwo(a, b) {
    return (a * b) / greatestCommonDivisor(a, b);
}

/**
 * @return {number}
 */
function Interpolate(start, end, steps, count){
    return  Math.floor(start + (((end - start) / steps) * count))
}

/**
 * @return {string}
 */
function InterpolateColor(steps, count){
    return "rgb(" + Interpolate(startColor.r, endColor.r, steps, count) + ", " +
        Interpolate(startColor.g, endColor.g, steps, count) + ", " +
        Interpolate(startColor.b, endColor.b, steps, count) + ")";
}

function beautifyDate(date) {
    return $.datepicker.formatDate('DD, d MM, yy', new Date(date));
}

function simplifyDate(date){
    return $.datepicker.formatDate('m/d/yy', new Date(date));
}

$(function(){
    container = $('<div class="popup-container" >'+
        '<p><a id="info" clicked="">Information</a><a id="comparison">Comparison</a><a id="evolution">Evolution</a></p>' +
        "<div class='detailText tab-content' >" +
        ' This score of <span id="score"></span> points currently is on the <span id="rank"></span> rank and was created ' +
        'on <span id="date"></span> by <span id="name">.</span><br>' +
        "</div>" +
        "<div class='scoreChart generalChart tab-content'></div>" +
        "<div class='evolution tab-content'>"+
        "<div class='evolutionChart generalChart' >" +
        "</div>" +
        "<div class='evolution-legend'><div id='legend1' class='legend'></div><div id='legend2' class='legend'></div><div id='legend3' class='legend'></div></div>" +
        "</div></div>");

    $(document).on('mouseenter', '.ct-point', function() {
        tooltipHover(this);
        tooltip.removeClass('invis');
        tooltip.removeClass('invisible');
    });

    $(document).on('mouseleave', '.ct-point', function() {
        tooltip.addClass('invis');
    });

    $(document).on('mousemove', '.ct-point', function(event) {
        var r = ($('.ct-chart-line'))[0].getBoundingClientRect();

        tooltip.css({
            left: event.clientX - r.x,
            top: event.clientY - r.y
        });
    });


    temp = $(container);
    startColor = {r:0x80, g:0xff, b:0x03};
    endColor = {r:0xcc, g:0xdd, b:0xb8};
    yourColor = {r:0x31, g:0xbc, b:0x86};
    ws = new WebSocket(wsUri);

    ws.onerror = function(event){
        console.log("couldn't connect to socket...");
        console.log(event);
    };

    ws.onopen = function() {
        console.log("Connected to " + wsUri);
    };

    ws.onmessage = function(msg){
        console.log(msg);
        let data = msg.data;
        displayNotification('success', 'New HighScore from ' + data.split(';')[0] + ", " + data.split(';')[1] + " points", 4000);
    };

    document.onkeypress = function(key){
        switch(key.keyCode) {
            case 37: // left
                if(!$(_backSelector).prop('disabled'))
                $(_backSelector).click();
                break;
            case 39: // right
                if(!$(_nextSelector).prop('disabled'))
                $(_nextSelector).click();
                break;
            default: return; // exit this handler for other keys
        }
        key.preventDefault();
    };

    $('thead').click(function(){
        $('#sort-sound')[0].currentTime = 0.45;
        $('#sort-sound')[0].volume = 1;
        $('#sort-sound')[0].play();
    });

    $(document).ready(function () {
        $('#table').tablePopulator({
            fetch_url: _baseUrl,
            previous_button_selector: _backSelector,
            next_button_selector: _nextSelector,
            pagination_limit: _rowsPerPage,
            search_field_selector: "#search-input",
            row_mapper: function (json_element, row_element) {
                row_element[0] = json_element['rank'];
                row_element[1] = json_element['name'];
                row_element[2] = json_element['score'];
            },
            afterRender: function(json_data){
                $('#table').find('tbody').find('tr').each(function(index, item){
                    $(item).on('click', function(){
                        let seq = 0, delays = 80, durations = 500;

                        let f = function() {
                            temp.find('#info').attr('clicked', '');
                            temp.find('#comparison').removeAttr('clicked');
                            temp.find('#evolution').removeAttr('clicked');

                            temp.find('.detailText').removeClass('invisible');
                            temp.find('.scoreChart').addClass('invisible');
                            temp.find('.evolution').addClass('invisible');

                            temp.find('#rank').text(json_data[index]['rank'] + '.');
                            temp.find('#score').text(json_data[index]['score']);
                            temp.find('#name').text(json_data[index]['name']);
                            temp.find('#date').text(beautifyDate(json_data[index]['created']));
                            temp.find('span').css('color', ' rgb(' + yourColor.r + ',' + yourColor.g + ',' + yourColor.b + ')');
                        };
                        let selectedScore = json_data[index];
                        f();

                        //information code
                        temp.find('#info').click(function(){
                            if($('#info').is('[clicked]')){
                                return;
                            }
                            f();
                        });

                        //comparison code
                        temp.find('#comparison').click(function() {
                            if($('#comparison').is('[clicked]')){
                                return;
                            }
                            temp.find('.scoreChart').empty();
                            temp.find('#info').removeAttr('clicked');
                            temp.find('#comparison').attr('clicked', '');
                            temp.find('#evolution').removeAttr('clicked');

                            temp.find('.detailText').addClass('invisible');
                            temp.find('.scoreChart').removeClass('invisible');
                            temp.find('.evolution').addClass('invisible');

                            let data = {
                                labels: [],
                                series: [[]]
                            };

                            $.getJSON(_baseUrl + '/nameAndScore', {}, function (d) {
                                let maxIndex = 0;
                                let longestLabel = 0;
                                d.forEach(function (item) {
                                    if (item['name'] != selectedScore['name'] || item['score'] != selectedScore['score']) {
                                        data.labels.push(item['name']);
                                        data.series[0].push(item['score']);
                                        if(item['name'].length > longestLabel){
                                            longestLabel = item['name'].length;
                                        }
                                        maxIndex++;
                                    }
                                });
                                if('Selected Score'.length > longestLabel){
                                    longestLabel ='Selected Score'.length;
                                }
                                data.labels.push('Selected Score');
                                data.series[0].push(selectedScore['score']);
                                new Chartist.Bar(container.find('.scoreChart')[0], data, barChartOptions)
                                    .on('draw', function (data) {
                                        seq++;
                                        if (data.type === 'bar') {
                                            if (data.index == maxIndex) {
                                                data.element.attr({
                                                    style: 'stroke-width: 30px;' +
                                                    'stroke: rgb(' + yourColor.r + ',' + yourColor.g + ',' + yourColor.b + ')'
                                                });
                                            }
                                            else {
                                                data.element.attr({
                                                    style: 'stroke-width: 30px;' +
                                                    'stroke: ' + InterpolateColor(maxIndex, data.index)
                                                });
                                            }
                                            data.element.animate({
                                                opacity: {
                                                    // The delay when we like to start the animation
                                                    begin: seq * delays + 100,
                                                    // Duration of the animation
                                                    dur: durations,
                                                    // The value where the animation should start
                                                    from: 0,
                                                    // The value where it should end
                                                    to: 1
                                                }
                                            });
                                        } else if(data.type === 'label' && data.axis === 'y') {
                                            data.element.animate({
                                                x: {
                                                    begin: seq * delays,
                                                    dur: durations,
                                                    from: data.x - 100,
                                                    to: data.x,
                                                    easing: 'easeOutQuart'
                                                }
                                            });
                                        } else if(data.type === 'label' && longestLabel > 9){
                                            $(data.element).find('span').css('-ms-transform', 'rotate(45deg)')
                                                .css('-webkit-transform', 'rotate(45deg)').css('transform',  'rotate(45deg)');
                                        } else if(data.type === 'grid') {
                                            // Using data.axis we get x or y which we can use to construct our animation definition objects
                                            var pos1Animation = {
                                                begin: seq * delays,
                                                dur: durations,
                                                from: data[data.axis.units.pos + '1'] - 30,
                                                to: data[data.axis.units.pos + '1'],
                                                easing: 'easeOutQuart'
                                            };

                                            var pos2Animation = {
                                                begin: seq * delays,
                                                dur: durations,
                                                from: data[data.axis.units.pos + '2'] - 100,
                                                to: data[data.axis.units.pos + '2'],
                                                easing: 'easeOutQuart'
                                            };

                                            var animations = {};
                                            animations[data.axis.units.pos + '1'] = pos1Animation;
                                            animations[data.axis.units.pos + '2'] = pos2Animation;
                                            animations['opacity'] = {
                                                begin: seq * delays,
                                                dur: durations,
                                                from: 0,
                                                to: 1,
                                                easing: 'easeOutQuart'
                                            };

                                            data.element.animate(animations);
                                        }
                                    })
                                    .on('created', function(){
                                        seq = 0;
                                    })
                            });
                        });

                        //evolution code
                        temp.find('#evolution').click(function(){
                            if($('#evolution').is('[clicked]')){
                                return;
                            }
                            temp.find('.evolutionChart').empty();
                            temp.find('#info').removeAttr('clicked');
                            temp.find('#comparison').removeAttr('clicked');
                            temp.find('#evolution').attr('clicked', '');

                            temp.find('.detailText').addClass('invisible');
                            temp.find('.scoreChart').addClass('invisible');
                            temp.find('.evolution').removeClass('invisible');
                            temp.find('.evolution-legend').addClass('invisible');

                            $.getJSON(_baseUrl + '/similarScores/' +  selectedScore['name'], {}, function (d){
                                let names = [];
                                let data = {
                                    labels: [],
                                    series: []
                                };

                                let maxNumberOfScores = 0;
                                let scoresInKey = [];
                                for(let key in d){
                                    if (!d.hasOwnProperty(key)) { continue; }
                                    if(d[key].length > maxNumberOfScores)
                                        scoresInKey.push(d[key].length);
                                }
                                maxNumberOfScores = leastCommonMultiple(scoresInKey);

                                for(let i = 0; i <= maxNumberOfScores; i++ ){
                                    data.labels.push(i);
                                }

                                for(let key in d){
                                    if (!d.hasOwnProperty(key)) { continue; }
                                    let scores = d[key].sort(function(s1, s2){
                                        return s1[0] > s2[0];
                                    });
                                    let thisSeries = {
                                        name: key,
                                        data: []
                                    };
                                    names.push(key);
                                    thisSeries.data.push(0);

                                    let numberOfScores = scores.length;
                                    let xValues = maxNumberOfScores/numberOfScores;
                                    let lastY = 0;
                                    let tempY = 0;
                                    for(let i = 0; i < numberOfScores; i++){
                                        let yDiff = (scores[i][0]-lastY)/xValues;
                                        for(let c = 0; c < xValues; c++){
                                            tempY += yDiff;
                                            thisSeries.data.push(tempY);
                                        }
                                        lastY = scores[i][0];
                                    }
                                    data.series.push(thisSeries);
                                }

                                new Chartist.Line(container.find('.evolutionChart')[0], data, lineChartOptions).on('draw', function (data) {
                                    seq++;
                                    if (data.type === 'line') {
                                        data.element.animate({
                                            opacity: {
                                                // The delay when we like to start the animation
                                                begin: seq * delays + 100,
                                                // Duration of the animation
                                                dur: durations,
                                                // The value where the animation should start
                                                from: 0,
                                                // The value where it should end
                                                to: 1
                                            }
                                        });
                                        if (data.index == 0) {
                                            data.element.attr({
                                                style: 'stroke: rgb(' + yourColor.r + ',' + yourColor.g + ',' + yourColor.b + ')'
                                            });
                                        } else if (data.index == 1) {
                                            data.element.attr({
                                                style: 'stroke: rgb(' + startColor.r + ',' + startColor.g + ',' + startColor.b + ')'
                                            });
                                        } else if (data.index == 2) {
                                            data.element.attr({
                                                style: 'stroke: rgb(' + endColor.r + ',' + endColor.g + ',' + endColor.b + ')'
                                            });
                                        }
                                    } else if(data.type === 'label' && data.axis === 'y') {
                                        data.element.animate({
                                            x: {
                                                begin: seq * delays,
                                                dur: durations,
                                                from: data.x - 100,
                                                to: data.x,
                                                easing: 'easeOutQuart'
                                            }
                                        });
                                    } else if(data.type === 'grid') {
                                        // Using data.axis we get x or y which we can use to construct our animation definition objects
                                        var pos1Animation = {
                                            begin: seq * delays,
                                            dur: durations,
                                            from: data[data.axis.units.pos + '1'] - 30,
                                            to: data[data.axis.units.pos + '1'],
                                            easing: 'easeOutQuart'
                                        };

                                        var pos2Animation = {
                                            begin: seq * delays,
                                            dur: durations,
                                            from: data[data.axis.units.pos + '2'] - 100,
                                            to: data[data.axis.units.pos + '2'],
                                            easing: 'easeOutQuart'
                                        };

                                        var animations = {};
                                        animations[data.axis.units.pos + '1'] = pos1Animation;
                                        animations[data.axis.units.pos + '2'] = pos2Animation;
                                        animations['opacity'] = {
                                            begin: seq * delays,
                                            dur: durations,
                                            from: 0,
                                            to: 1,
                                            easing: 'easeOutQuart'
                                        };

                                        data.element.animate(animations);
                                    } else if(data.type === 'point' ){
                                        data.element.animate({
                                            opacity: {
                                                begin: seq * delays + 200,
                                                dur: durations,
                                                from: 0,
                                                to: 1
                                            }
                                        });

                                        let isComputedPoint = true;
                                        let val = Math.round(data.series.data[data.index]*100)/100;
                                        let scores = d[data.series.name];
                                        scores.forEach(function(it){
                                            if(val == it[0]){
                                                isComputedPoint = false;
                                            }
                                        });
                                        if(isComputedPoint){
                                            data.element.addClass('invisible');
                                        }
                                        else{
                                            if (names[0] == data.series.name) {
                                                data.element.attr({
                                                    style: 'stroke: rgb(' + yourColor.r + ',' + yourColor.g + ',' + yourColor.b + ')'
                                                });
                                            } else if (names[1] == data.series.name) {
                                                data.element.attr({
                                                    style: 'stroke: rgb(' + startColor.r + ',' + startColor.g + ',' + startColor.b + ')'
                                                });
                                            } else{
                                                data.element.attr({
                                                    style: 'stroke: rgb(' + endColor.r + ',' + endColor.g + ',' + endColor.b + ')'
                                                });
                                            }
                                        }
                                    }
                                }).on('created', function () {
                                    seq = 0;

                                    names.forEach(function(it, index){
                                        let text = '';
                                        if(it.length > 10){
                                            for(let c = 0; c < 8; c++){
                                                text = text + it[c];
                                            }
                                            text = text + "...";
                                        } else{
                                            text = it;
                                        }
                                        container.find('#legend'+(index+1)).text(text);
                                    });
                                    container.find('#legend1').css('background-color', 'rgba(' + yourColor.r + ',' + yourColor.g + ',' + yourColor.b + ', 0.6)');
                                    container.find('#legend2').css('background-color', 'rgba(' + startColor.r + ',' + startColor.g + ',' + startColor.b + ',0.4)');
                                    container.find('#legend3').css('background-color', 'rgba(' + endColor.r + ',' + endColor.g + ',' + endColor.b + '),0.6');

                                    temp.find('.evolution-legend').removeClass('invisible');

                                    if(tooltip === 0) {
                                        temp.find('.evolution-legend').append($('<div id="tooltip" class="invis"></div>'));
                                    }
                                    tooltip = $('#tooltip');
                                    tooltipHover = function(self) {
                                        let name = $(self).closest('.ct-series').attr('ct:series-name'),
                                            val = $(self).attr('ct:value'), created = null;
                                        d[name].forEach(function(item){
                                            if(item[0] == val){
                                                created = item[1];
                                            }
                                        });

                                        if(created != null) {
                                            tooltip.text(val + " points by " + name + " on " + simplifyDate(created));
                                        }
                                    };
                                });
                            });

                        });
                    });

                    $(item).avgrund({
                        width: 600,
                        height: 400,
                        showClose: false,
                        template: temp[0],
                        holderClass: 'container'
                    });

                })
            }

        });

    });

});