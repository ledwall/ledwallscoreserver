package at.htl.rest;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;

/**
 * Created by stoez on 12.05.2016.
 */
@ApplicationPath("rs")
public class RestConfig extends Application {

}
