package at.htl.rest;

import at.htl.business.MessageBroadcaster;
import at.htl.business.SnakeScoreFacade;
import at.htl.entity.SnakeScore;
import com.google.gson.Gson;

import javax.inject.Inject;
import javax.json.JsonObject;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.*;

/**
 * Created by stoez on 12.05.2016.
 */
@Path("snake/")
public class SnakeScoreResource {
    @Inject
    SnakeScoreFacade snakeScoreFacade;

    @PersistenceContext
    EntityManager em;

    @GET
    @Path("forApp")
    @Produces("application/json")
    public List<SnakeScore> getForApp(){
        Query q = em.createNamedQuery("SnakeScore.findAllOrdered", SnakeScore.class).setMaxResults(20);

        List<SnakeScore> res = q.getResultList();
        res.forEach(( s -> s.setRank(
                Math.toIntExact(
                        em.createNamedQuery("SnakeScore.getRankFromScore", Long.class)
                                .setParameter(1, s.getScore())
                                .getSingleResult())
                        +1))
        );
        return res;
    }

    @GET
    @Produces("application/json")
    public List<SnakeScore> get(@Context UriInfo ui) {
        MultivaluedMap<String, String> queryParams = ui.getQueryParameters();
        String pattern = "";

        StringBuilder queryString = new StringBuilder("select s from SnakeScore s");
        if (queryParams.containsKey("query") && !queryParams.getFirst("query").isEmpty()) {
            queryString.append(" where s.name like :PATTERN ");
            pattern = queryParams.getFirst("query");
        }
        if (queryParams.containsKey("sort") && queryParams.containsKey("order_by")) {
            String order = "";
            String ascdesc = queryParams.getFirst("sort");

            if (queryParams.getFirst("order_by").equals("id") || Objects.equals(queryParams.getFirst("order_by"), "score") ||
                    Objects.equals(queryParams.getFirst("order_by"), "name") || queryParams.getFirst("order_by").equals("created")) {
                order = queryParams.getFirst("order_by");
                if (!Objects.equals(order, "")) {
                    queryString.append(" order by s.").append(order).append(Objects.equals(ascdesc, "ASC") ? " asc " : " desc ");
                } else{
                    queryString.append(" order by s.score desc ");
                }
            } else{
                queryString.append(" order by s.score desc ");
            }
        }
        Query q = em.createQuery(queryString.toString(), SnakeScore.class);

        if (!pattern.isEmpty())
            q.setParameter("PATTERN", '%'+ pattern + '%');
        int skip = 0;
        int limit = 7;
        try {
            limit = Integer.parseInt(queryParams.getFirst("limit"));
        } catch (NumberFormatException ex) {
            System.out.println("no limit for data-lines given");
        }
        try{
            skip = Integer.parseInt(queryParams.getFirst("skip"));
        }catch (NumberFormatException ex){
            System.out.println("no skip value given");
        }
        q.setMaxResults(limit);
        q.setFirstResult(skip);
        List<SnakeScore> res = q.getResultList();
        res.forEach(( s -> s.setRank(
                Math.toIntExact(
                em.createNamedQuery("SnakeScore.getRankFromScore", Long.class)
                        .setParameter(1, s.getScore())
                        .getSingleResult())
                +1))
        );
        return res;
    }

    @GET
    @Path("{name}")
    @Produces("application/json")
    public List<SnakeScore> getByName(@PathParam("name") String name ){
        return snakeScoreFacade.findAllWithNameSorted(name);
    }

    @GET
    @Path("similarScores/{name}")
    @Produces("applicaiton/json")
    public String getSimilarCharts(@PathParam("name") String name){
        Gson gson = new Gson();
        HashMap<String, List<Object[]>> res = new HashMap();
        Set<String> names = new HashSet<>();
        names.add(name);
        int i = 0,  num = 0;
        int max = em.createNamedQuery("SnakeScore.countAll", Long.class).getSingleResult().intValue();
        while(i < 2){
            SnakeScore s = em.createNamedQuery("SnakeScore.findAllOrdered", SnakeScore.class).setFirstResult(num++).setMaxResults(1).
                    getResultList().get(0);
            if(!names.contains(s.getName())){
                names.add(s.getName());
                i++;
            }
            if(num == max){
                break;
            }
        }

        HashMap<String, List<Object[]>> temp = new HashMap<>();
        names.forEach(n ->temp.put(n, em.createNamedQuery("SnakeScore.findBestScoresWithName", Object[].class).
                setParameter("NAME", n).setMaxResults(5).getResultList()));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
        temp.forEach((k,v) ->{
            List<Object[]> values = new ArrayList<>(v.size());
            v.forEach(d ->{
                Object[] objects = new Object[2];
                objects[0] = d[0];
                objects[1] = sdf.format((Date)d[1]);
                values.add(objects);
            });
            res.put(k, values);
        });
        return gson.toJson(res);
    }

    @POST
    @Consumes("application/json")
    public Response create(JsonObject obj) {
        try {
            SnakeScore score = new SnakeScore(obj);

            //check if score already exists
            if (snakeScoreFacade.countWithSameNameAndScore(score.getName(), score.getScore()) == 0) {
                snakeScoreFacade.save(score);
                System.out.println("Reached score: " + score.getScore());
                //return created if a good score has been sent
                MessageBroadcaster.Companion.broadcastMessage(score.getName() + ";" + score.getScore());

                return Response.status(Response.Status.CREATED).build();
            }
            //return bad request if the score was already in the database
            return Response.status(Response.Status.BAD_REQUEST).build();
        } catch (Exception ex) {
            //on any thrown exception return an internal server error
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    @GET
    @Path("nameAndScore")
    @Produces("application/json")
    public List<SnakeScore> getBestNames(){
        List<SnakeScore> scores = new ArrayList<>(5);
        int i = 0, num = 0;
        int max = em.createNamedQuery("SnakeScore.countAll", Long.class).getSingleResult().intValue();
        while(i < 5){
            SnakeScore s = em.createNamedQuery("SnakeScore.findAllOrdered", SnakeScore.class).setFirstResult(num++).setMaxResults(1).
                    getResultList().get(0);
            if(!scores.stream().anyMatch(anyScore -> anyScore.getName().toLowerCase().matches(s.getName().toLowerCase()))){
                scores.add(s);
                i++;
            }
            if(num == max)
                break;
        }
        return scores;
    }
}
