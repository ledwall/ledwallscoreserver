package at.htl.business;

import at.htl.entity.SnakeScore;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ejb.Startup;
import javax.inject.Inject;
import java.sql.Date;

/**
 * Created by stoez on 12.05.2016.
 */
@Startup
@Singleton
public class InitBean {
    @Inject
    SnakeScoreFacade snakeScoreFacade;

    @PostConstruct
    public void init(){
        if(snakeScoreFacade.findAll().size()==0){
            SnakeScore score1 = new SnakeScore(5, "Stoez", new Date(2015 - 1900, 3, 12));
            SnakeScore score2 = new SnakeScore(11, "Stoez", new Date(2015 - 1900, 4, 27));
            SnakeScore score3 = new SnakeScore(13, "Tanzerix", new Date(2015 - 1900, 5, 2));
            SnakeScore score4 = new SnakeScore(10, "Markus", new Date(2016 - 1900, 4, 7));
            SnakeScore score5 = new SnakeScore(7, "Markus", new Date(2016 - 1900, 4, 15));
            SnakeScore score6 = new SnakeScore(21, "Tanzerix", new Date(2016 - 1900, 4, 14));


            snakeScoreFacade.save(score1);
            snakeScoreFacade.save(score2);
            snakeScoreFacade.save(score3);
            snakeScoreFacade.save(score4);
            snakeScoreFacade.save(score5);
            snakeScoreFacade.save(score6);
        }
        System.out.println("Created default score data...");
    }
}
