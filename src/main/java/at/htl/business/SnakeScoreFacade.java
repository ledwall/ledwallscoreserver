package at.htl.business;

import at.htl.entity.SnakeScore;

import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;
import java.util.List;

/**
 * Created by stoez on 12.05.2016.
 */
@Stateless
public class SnakeScoreFacade {
    @PersistenceContext(unitName = "myPU")
    EntityManager em;

    public List<SnakeScore> findAll(){
        return em.createNamedQuery("SnakeScore.findAll", SnakeScore.class).
            getResultList();
    }

    public List<SnakeScore> findAllSorted(int numberOfRows){
        List<SnakeScore> res =  em.createNamedQuery("SnakeScore.findAllOrdered", SnakeScore.class)
            .setMaxResults(numberOfRows).getResultList();
        res.forEach(s ->s.setRank(
                Math.toIntExact(
                        em.createNamedQuery("SnakeScore.getRankFromScore", Long.class)
                                .setParameter(1, s.getScore())
                                .getSingleResult())
                        +1));
        return res;
    }

    public SnakeScore save(SnakeScore snakeScore){
        return em.merge(snakeScore);
    }

    public List<SnakeScore> findAllWithNameSorted(String name){
        return em.createNamedQuery("SnakeScore.findBestWithName", SnakeScore.class)
            .setParameter("NAME", name)
            .getResultList();
    }

    public SnakeScore findBestWithName(String name){
        return em.createNamedQuery("SnakeScore.findBestWithName", SnakeScore.class)
            .setParameter("NAME", name)
            .setMaxResults(1)
            .getResultList()
            .get(0);
    }

    public List<SnakeScore> findBestFromTo(int from, int to){
        return em.createNamedQuery("SnakeScore.findAllOrdered", SnakeScore.class)
            .setMaxResults(to)
            .getResultList()
            .subList(from, to);
    }

    public List<SnakeScore> findBestFromToWithName(String name, int from, int to){
        return em.createNamedQuery("SnakeScore.findBestWithName", SnakeScore.class)
            .setParameter("NAME", name)
            .setMaxResults(to)
            .getResultList()
            .subList(from, to);
    }

    public int countWithName(String name){
        return em.createNamedQuery("SnakeScore.countWithName").
            setParameter("NAME", name).
            getFirstResult();
    }

    public int countAll(){
        return em.createNamedQuery("SnakeScore.countAll")
            .getFirstResult();
    }

    public int countWithSameNameAndScore(String name, int score){
        return em.createNamedQuery("SnakeScore.countWithSameNameAndScore")
            .setParameter("NAME", name)
            .setParameter("SCORE", score)
            .getFirstResult();
    }
}
