package at.htl.business

import javax.websocket.OnClose
import javax.websocket.OnMessage
import javax.websocket.OnOpen
import javax.websocket.Session
import javax.websocket.server.ServerEndpoint
import java.util.Collections
import java.util.HashSet

fun println(a: String) = System.out.println(a)

@ServerEndpoint("/socket")
class MessageBroadcaster {

    @OnMessage
    fun onMessage(message: String): String? {
        println(message)
        return null
    }

    @OnOpen
    fun onOpen(peer: Session) {
        peers.add(peer)
        println("Peer added")
    }

    @OnClose
    fun onClose(peer: Session) {
        peers.remove(peer)
        println("Peer removed")
    }

    companion object {
        private val peers = Collections.synchronizedSet(HashSet<Session>())

        fun broadcastMessage(msg: String) {
            println("broadcasting message to " + peers.size + " peers: " + msg)
            for (peer in peers) {
                peer.basicRemote.sendText(msg)
            }
        }
    }
}
