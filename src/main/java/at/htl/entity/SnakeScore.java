package at.htl.entity;

import javax.json.JsonObject;
import javax.persistence.*;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.sql.Date;

/**
 * Created by stoez on 12.05.2016.
 */
@Entity
@NamedQueries({
        @NamedQuery(name = "SnakeScore.findAll", query = "select s from SnakeScore s"),
        @NamedQuery(name = "SnakeScore.countAll", query = "select count(s) from SnakeScore s"),
        @NamedQuery(name = "SnakeScore.findAllOrdered", query = "select s from SnakeScore s order by s.score desc"),
        @NamedQuery(name = "SnakeScore.countWithName", query = "select count(s) from SnakeScore s where s.name = :NAME"),
        @NamedQuery(name = "SnakeScore.findBestWithName", query = "select s from SnakeScore s where s.name = :NAME order by s.score desc"),
        @NamedQuery(name = "SnakeScore.findBestScoresWithName", query = "select s.score, s.created from SnakeScore s where s.name = :NAME order by s.score desc"),
        @NamedQuery(name = "SnakeScore.countWithSameNameAndScore", query = "select count(s) from SnakeScore s where s.name = :NAME and s.score = :SCORE"),
        @NamedQuery(name = "SnakeScore.getRankFromScore", query = "select count(s) from SnakeScore s where s.score > ?1 "),
        @NamedQuery(name = "SnakeScore.getNames", query = "select distinct(s.name) from SnakeScore s"),
        @NamedQuery(name = "SnakeScore.getNameAndScore", query = "select a from SnakeScore a where a.name = :NAME and " +
                "a.score = (select max(s.score) from SnakeScore s where s.name = :NAME)")
})
@XmlRootElement
@Table(name = "SM_SNAKESCORE")
public class SnakeScore implements Serializable{
    private static long SERIALIZED_VERSION = 1L;

    @Id @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "SS_ID")
    private int id;

    @Column(name = "SS_SCORE")
    private int score;

    @Column(name = "SS_NAME")
    private String name;

    @Column(name = "SS_DATE")
    private Date created;

    @Transient
    private int rank;

    public SnakeScore(JsonObject jsonObject) {
        this.score = jsonObject.getInt("score");
        String n = jsonObject.getString("name");

        String dateParts[] = jsonObject.getString("created").split("-");
        int year = Integer.parseInt(dateParts[0])-1900;
        int month = Integer.parseInt(dateParts[1]);
        int day = Integer.parseInt(dateParts[2]);

        //noinspection deprecation
        this.created = new Date(year, month, day);
        StringBuilder b = new StringBuilder().append(Character.toUpperCase(n.charAt(0)));
        for(int i = 1; i < n.length(); i++){
            if(n.charAt(i-1) == ' '){
                b.append(Character.toUpperCase(n.charAt(i)));
            }
            else{
                b.append(Character.toLowerCase(n.charAt(i)));
            }
        }
        this.name = b.toString();
    }

    public SnakeScore(int id, int score, String name, Date created, int rank) {
        this.id = id;
        this.score = score;
        this.name = name;
        this.created = created;
        this.rank = rank;
    }

    public int getRank() {
        return rank;
    }

    public void setRank(int rank) {
        this.rank = rank;
    }

    public int getId() {
        return id;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public SnakeScore(int score, String name, Date created) {
        this.score = score;
        this.name = name;
        this.created = created;
    }

    public SnakeScore() {
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }
}
